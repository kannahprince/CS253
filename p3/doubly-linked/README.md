# Project 3: Linked List

* Author: Prince Kannah
* Class: CS253 Section 3

## Overview:

The main objective of P3 was implementing a doubly linked using pointers and dynamic memory allocation. The use of pointers include struct pointers as well
as function pointers. An assertion macro is then use to test the code.

## Compiling and Using:

The folders and respective files included with this program:
* libsrc - doubly linked source code 
    * List.c
    * List.h
    * Node.c
    * Node.h
* testsuite - doubly linked test code
    * Object.c
    * Object.h
    * UnitTest.c
    * RandomTestList.c
    * SimpleTest.c
    * Makefile
* Makefile - gcc makefile.
* run.sh - runs all test suite code and Valgrind and GDB.
* backpack.sh - auto grader use to check the correctness of the application.
* grader - auto grader use to check the correctness of the application.
* doxygen-config.

**_NOTE_: All program files should be in the same directory.**

To run the test programs you will need to set the paths to find the library:

`export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./lib`

Then run the test programs as follows:

`testsuite/UnitTestList`

## Discussion:

The biggest challenge I had was understanding the nature of pointers and importantly working with them to implement a DLL. My approach was to get all the easy functions out of the way to have more time to focus on the harder ones. I first worked on size then isEmpty. Next I focused on add to the list. For addAtFront and addAtRear, I drew pictures to help me understand what needed to happen in both cases.

The trickiest part (at least for me, anyways) was figuring out how to reverse the list. My initial thought was to create a new list and copy the original list starting from tail. This was until I realized that the function had a void return type. Then I thought about using a flag that to keep track of when reverseList has been call. This flag is then checked in the other functions on subsequent calls and act accordingly.

With the help of a TA (and online sources), I was able to understand that what I needed to do was switch the next and previous pointers of each node to correctly reverse the list.

Once I started coding more and more with pointers it started making sense. With pointers we can interact with the list in place without creating extraneous nodes and pointers. This makes the program cleaner but most importantly, running efficiently.

## Testing:

Testing was done in tandem with the code writing. I was especially afraid of having SEGFAULT and dangling pointers. So, before I coded a function, I wrote a test for it in my UnitTest file. This made debugging easier as I made sure all bugs were squashed before moving onto a new function. This project also helped me get even more familiar with C-debugging tools like Valgrind and GDB. I use GDB whenever I my UnitTest failed with a SEGFAULT.

Valgrind was extremely helpful in helping realize that there was memory leaks in my UnitTest file. Whenever I tested a method that returned a pointer I got a memory leak. This was because I was not freeing the node that is created when the pointer is returned. Doing this cleared up the memory leak errors in my UnitTest file.

In addition to the UnitTest I also ran the provided SimpleTest and RandomTest. The RandomTest was helpful in tracking down bugs that could have take forever to find.

## Sources used:

I utilized the Piazza posts to help me when I got stuck. Outside class sources I use include:

* [Java DLL class](http://www.spatial.cs.umn.edu/Courses/Spring12/1902/labs/lab8/DLList.java) - this Java class was helpful especially when it came to reversing the list.
* [Three different ways to reverse DLL](http://www.codeproject.com/Articles/27742/How-To-Reverse-a-Linked-List-Different-Ways) - this give the me idea on the different ways I could have approach reversing my list. It would have been cool to do it recusively.
* [Reversing a DLL - Stack overflow](http://stackoverflow.com/questions/11166968/reversing-a-doubly-linked-list) - the pictures provided made it easy to understand what needed to happen at each node in order to reverse the list.
