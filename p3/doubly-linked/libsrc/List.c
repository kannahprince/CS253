#include <stdio.h>
#include <stdlib.h>
#include "List.h"

struct list * createList(int (*equals)(const void *,const void *),
                         char * (*toString)(const void *),
                         void (*freeObject)(void *)) {
    struct list *list;
    list = (struct list *) malloc(sizeof(struct list));
    list->size = 0;
    list->head = NULL;
    list->tail = NULL;
    list->equals = equals;
    list->toString = toString;
    list->freeObject = freeObject;
    return list;
}

void freeList(struct list *list)
{
    if(list == NULL) return;

    //list exists but there's nothing in it
    if(isEmpty(list))
    {
        free(list);
        list = NULL;
    }
    else
    {
        //List is not empty!
        struct node *currentNode = list->head;
        while (currentNode != NULL) {
            struct node *nextNode = currentNode->next;
            freeNode(currentNode, list->freeObject);
            currentNode = nextNode;
        }
        free(list);
        list = NULL;
    }
}

int getSize(const struct list *list)
{
    return list == NULL ? 0 : list->size;
}

int isEmpty(const struct list *list)
{
    return list == NULL ? 0: list->size == 0;
}

void addAtFront(struct list *list, struct node *node)
{
    if (list == NULL) return;
    if (node == NULL) return;

    if (list->head == NULL)
    {
        list->head = node;
        list->tail = node;
    }
    else
    {
        node->next = list->head;
        list->head->prev = node;
        list->head = node;
        list->head->prev = NULL;
    }
    list->size++;
}

void addAtRear(struct list *list, struct node *node)
{
    if(list == NULL) return;
    if(node == NULL) return;

    if(list->tail == NULL)
    {
        list->tail = node;
        list->head = node;
    }
    else
    {
        node->prev = list->tail;
        list->tail->next = node;
        list->tail = node;
        list->tail->next = NULL;
    }
    list->size++;
}

struct node* removeFront(struct list *list)
{
	if(list == NULL) return NULL;
    if(isEmpty(list)) return NULL;

    if(list->head == list->tail) //list is size 1
    {
        struct node *node = list->head;
        list->head = NULL;
        list->tail = NULL;
        list->size--;
        return node;
    }
    else
    {
        struct node *node = list->head;
        list->head = list->head->next;
        list->head->prev = NULL;
        node->next = NULL;
        list->size--;
        return node;
    }
}

struct node* removeRear(struct list *list) {

	if(list == NULL) return NULL;
	if(isEmpty(list)) return NULL;

    if(list->tail == list->head)
    {
        struct node *node = list->tail;
        list->tail = NULL;
        list->head = NULL;
        list->size--;
        return node;
    }
    else
    {
        struct node *node = list->tail;
        list->tail = list->tail->prev;
        list->tail->next = NULL;
        node->prev = NULL;
        list->size--;
        return node;
    }
}


struct node* removeNode(struct list *list, struct node *node)
{	
	if(list == NULL) return NULL;
    if(node == NULL) return NULL;
    if(isEmpty(list)) return NULL;

    if(node == list->head)
    {
        return removeFront(list);
    }
    else if(node == list->tail)
    {
        return removeRear(list);
    }
    else
    {
        node->prev->next = node->next; //establish forward connection
        node->next->prev = node->prev; //establish backward connection
        node->next = NULL; //Delete all connections to node
        node->prev = NULL;
    }
    list->size--;
    return node;
}

struct node* search(const struct list *list, const void *obj)
{
   if(list == NULL) return NULL;
   if(isEmpty(list)) return NULL;
   if(obj == NULL) return NULL;

    struct node * objectNode = list->head;
    while(objectNode != NULL)
    {
        if(list->equals(objectNode->obj,obj))
        {
            return objectNode;
        }
        objectNode = objectNode->next;
    }
    //if a node with the given object is not found
    return NULL;
}

void reverseList(struct list *list)
{
    if(list == NULL) return;
    struct node *currentNode = list->head;
    struct node *oldHead = list->head;

    struct node *previousNode = NULL;
    while (currentNode != NULL)
    {
        previousNode = currentNode->prev;
        currentNode->prev = currentNode->next;
        currentNode->next = previousNode;
        currentNode = currentNode->prev;
    }
    //swap new heads and tail
    list->head = list->tail;
    list->tail = oldHead;
}

void printList(const struct list *list)
{
    if (!list) return; //list was null!!
    int count = 0;
    char *output;
    struct node *temp = list->head;
    while (temp) {
        output = list->toString(temp->obj);
        printf(" %s -->",output);
        free(output);
        temp = temp->next;
        count++;
        if ((count % 6) == 0)
            printf("\n");
    }
    printf(" NULL \n");
}
