/*
 * UnitTestList.c
 *
 *      Author: marissa
 */

#include <stdio.h>
#include <stdlib.h>

#include "Object.h"
#include "Node.h"
#include "List.h"

/*
 * macro to mimic the functionality of assert() from <assert.h>. The difference is that this version doesn't exit the program entirely.
 * It will just break out of the current function (or test case in this context).
 */
#define myassert(expr) if(!(expr)){ fprintf(stderr, "\t[assertion failed] %s: %s\n", __PRETTY_FUNCTION__, __STRING(expr)); return 0; }

struct list *testlist;

int testCount = 0;
int passCount = 0;

void printTestInfo(char* testName, char *info)
{
    fprintf(stdout, "%s - %s\n", testName, info);
}

void printTestResult(char* testName, int passed)
{
    if(passed) {
        fprintf(stdout, "%s - %s\n\n", "[PASSED]", testName);
    } else {
        fprintf(stdout, "%s - %s\n\n", "[FAILED]", testName);
    }
}

struct node *createTestNode(int jobid)
{
    struct object * job = createObject(jobid, "cmd args");
    struct node *node = createNode(job);
    return node;
}

void beforeTest(char* testName)
{
    printTestInfo(testName, "Running...");
    testlist = createList(equals, toString, freeObject);
    testCount++;
}

void afterTest(char* testName, int result)
{
    printTestResult(testName, result);
    freeList(testlist);
    passCount += result;
}

/*
 * TEST: addAtFrontWithNoNodes
 * Beginning state:
 * HEAD == NULL
 * TAIL == NULL
 * SIZE == 0
 *
 * Ending state:
 * [1]
 * HEAD == [1]
 * TAIL == [1]
 * SIZE == 1
 */
int addAtFrontWithNoNodes()
{
    struct node *node = createTestNode(1);
    addAtFront(testlist, node);

    //List state
    myassert(testlist->size == 1);
    myassert(testlist->head == node);
    myassert(testlist->tail == node);
    //Head connection
    myassert(testlist->head->next == NULL);
    //Tail connection
    myassert(testlist->head->prev == NULL);
    printList(testlist);
    return 1;
}

/*
 * TEST: addAtFrontWithNoeNode
 * Beginning state:
 * [1]
 * HEAD == [1]
 * TAIL == [1]
 * SIZE == 1
 *
 * Ending state:
 * [2][1]
 * HEAD == [2]
 * TAIL == [1]
 * SIZE == 2
 */
int addAtFrontWithOneNode()
{
    struct node *node = createTestNode(1);
    addAtFront(testlist, node);
    struct node *node2 = createTestNode(1);
    addAtFront(testlist, node2);

    //List state
    myassert(testlist->size == 2);
    myassert(testlist->head == node2);
    myassert(testlist->tail == node);

    //Head connection
    myassert(testlist->head->next == testlist->tail);
    myassert(testlist->head->prev == NULL);

    //Tail connection
    myassert(testlist->tail->next == NULL);
    myassert(testlist->tail->prev == testlist->head);
    printList(testlist);
    return 1;
}

/*
 * TEST: addAtRearWithNoNodes
 * Beginning state:
 * HEAD == NULL
 * TAIL == NULL
 * SIZE == 0
 *
 * Ending state:
 * [1]
 * HEAD == [1]
 * TAIL == [1]
 * SIZE == 1
 */
int addAtRearWithNoNodes()
{
    struct node *node = createTestNode(1);
    addAtRear(testlist, node);

    //List state
    myassert(testlist->size == 1);
    myassert(testlist->head == node);
    myassert(testlist->tail == node);

    //Head connection
    myassert(testlist->tail->next == NULL);
    //Tail connection
    myassert(testlist->tail->prev == NULL);
    printList(testlist);
    return 1;
}

/*
 * TEST: addAtRearWithOneNode
 * Beginning state:
 * [1]
 * HEAD == [1]
 * TAIL == [1]
 * SIZE == 1
 *
 * Ending state:
 * [1][2]
 * HEAD == [1]
 * TAIL == [2]
 * SIZE == 2
 */
int addAtRearWithOneNode()
{
    struct node *node = createTestNode(1);
    addAtRear(testlist, node);
    struct node *node2 = createTestNode(1);
    addAtRear(testlist, node2);

    //List state
    myassert(testlist->size == 2);
    myassert(testlist->head == node);
    myassert(testlist->tail == node2);

    //Head connection
    myassert(testlist->head->next == testlist->tail);
    myassert(testlist->head->prev == NULL);
    //Tail connection
    myassert(testlist->tail->next == NULL);
    myassert(testlist->tail->prev == testlist->head);
    printList(testlist);
    return 1;
}

/*
 * TEST: removeFromListWithOneNode
 * Beginning state:
 * [1]
 * HEAD == [1]
 * TAIL == [1]
 * SIZE == 1
 *
 * Ending state:
 * HEAD == NULL
 * TAIL == NULL
 * SIZE == 0
 */
int removeFromListWithOneNode()
{
    struct node *node = createTestNode(1);
    addAtFront(testlist, node);
    struct node *ret = removeNode(testlist, node);
	

    //List state
    myassert(testlist->size == 0);
    //Head connection
    myassert(testlist->head == NULL);
    //Tail connection
    myassert(testlist->tail == NULL);
    myassert(ret == node);
    printList(testlist);
	freeNode(ret,testlist->freeObject);
    return 1;
}

/*
 * TEST: nullNodeTest (addToFront, addToRear & removeNode)
 * Beginning state:
 * [1]
 * HEAD == [1]
 * TAIL == [1]
 * SIZE == 1
 *
 * Ending state:
 * [1]
 * HEAD == [1]
 * TAIL == [1]
 * SIZE == 1
 * EXPECTED == NULL
 */
int nullNodeTest()
{
    struct node *node = createTestNode(1);
    struct node *node1 = NULL;
    addAtFront(testlist, node);

    addAtFront(testlist,node1);
    addAtRear(testlist,node1);
    struct node *remove = removeNode(testlist,node1);

    //List state
    myassert(testlist->size == 1);
    //Head connection
    myassert(testlist->head ==  node);
    //Tail connection
    myassert(testlist->tail == node);
    myassert(remove == NULL);
    printList(testlist);
	freeNode(remove,testlist->freeObject);
    return 1;
}

/*
 * TODO: Write your test functions here
 */

/*
 * TEST: removeFrontWithNoNode
 * Beginning state:
 * HEAD == NULL
 * TAIL == NULL
 * SIZE == 0
 *
 * Ending state:
 * HEAD == NULL
 * TAIL == NULL
 * SIZE == 0
 * EXPECTED == NULL
 */
int removeFrontWithNoNode()
{
    struct node *ret = removeFront(testlist);

    //List state
    myassert(testlist->size == 0);
    //Head connection
    myassert(testlist->head == NULL);
    //Tail connection
    myassert(testlist->tail == NULL);
    myassert(ret == NULL);
    printList(testlist);
	freeNode(ret, testlist->freeObject);
    return 1;
}

/*
 * TEST: removeFrontWithOneNode
 * Beginning state:
 * [1]
 * HEAD == [1]
 * TAIL == [1]
 * SIZE == 1
 *
 * Ending state:
 * HEAD == NULL
 * TAIL == NULL
 * SIZE == 0
 * EXPECTED == 1
 */
int removeFrontWithOneNode()
{
    struct node *node = createTestNode(1);
    addAtRear(testlist,node);
    struct node *ret = removeFront(testlist);

    //List state
    myassert(testlist->size == 0);
    //Head connection
    myassert(testlist->head == NULL);
    //Tail connection
    myassert(testlist->tail == NULL);
    myassert(ret == node);
    printList(testlist);
	freeNode(ret, testlist->freeObject);
    return 1;
}

/*
 * TEST: removeFrontWithTwoNodes
 * Beginning state:
 * [1][2]
 * HEAD == [1]
 * TAIL == [2]
 * SIZE == 2
 *
 * Ending state:
 * HEAD == [2]
 * TAIL == [2]
 * SIZE == 1
 * EXPECTED == 1
 */
int removeFrontWithTwoNodes()
{
    struct node *node = createTestNode(1);
    struct node *node1 = createTestNode(2);
    addAtRear(testlist,node);
    addAtRear(testlist,node1);

    struct node *ret = removeFront(testlist);

    //List state
    myassert(testlist->size == 1);
    //Head connection
    myassert(testlist->head == node1);
    //Tail connection
    myassert(testlist->tail == node1);
    myassert(ret == node);
    printList(testlist);
	freeNode(ret,testlist->freeObject);
    return 1;
}

/*
 * TEST: removeRearWithNoNode
 * Beginning state:
 * HEAD == NULL
 * TAIL == NULL
 * SIZE == 0
 *
 * Ending state:
 * HEAD == NULL
 * TAIL == NULL
 * SIZE == 0
 * EXPECTED == NULL
 */
int removeRearWithNoNode() 
{
    struct node *node = removeRear(testlist);
    //List state
    myassert(testlist->size == 0);
    //Head connection
    myassert(testlist->head == NULL);
    //Tail connection
    myassert(testlist->tail == NULL);
    myassert(node == NULL);
    printList(testlist);
	freeNode(node, testlist->freeObject);
    return 1;
}

/*
 * TEST: removeRearWithOneNode
 * Beginning state:
 * [1]
 * HEAD == [1]
 * TAIL == [1]
 * SIZE == 1
 *
 * Ending state:
 * HEAD == NULL
 * TAIL == NULL
 * SIZE == 0
 * EXPECTED = 1
 */
int removeRearWithOneNode()
{
    struct node *node = createTestNode(1);
    addAtFront(testlist,node);

    struct node *ret = removeRear(testlist);

    //List state
    myassert(testlist->size == 0);
    //Head connection
    myassert(testlist->head == NULL);
    //Tail connection
    myassert(testlist->tail == NULL);
    myassert(ret == node);
    printList(testlist);
	freeNode(ret, testlist->freeObject);
    return 1;
}

/*
 * TEST: removeRearWithTwoNodes
 * Beginning state:
 * [2][1]
 * HEAD == [2]
 * TAIL == [1]
 * SIZE == 2
 *
 * Ending state:
 * HEAD == [2]
 * TAIL == [2]
 * SIZE == 1
 * EXPECTED == 1
 */
int removeRearWithTwoNodes()
{
    struct node *node = createTestNode(1);
    struct node *node1 = createTestNode(2);

    addAtFront(testlist, node);
    addAtFront(testlist,node1);
    struct node *ret = removeRear(testlist);

    //List state
    myassert(testlist->size == 1);
    //Head connection
    myassert(testlist->head == node1);
    //Tail connection
    myassert(testlist->tail == node1);
    myassert(ret == node);
    printList(testlist);
	freeNode(ret, testlist->freeObject);
    return 1;
}

/*
 * TEST: removeFromListWithMultiNodes
 * Beginning state:
 * [1][2][3][4][5]
 * HEAD == [1]
 * TAIL == [5]
 * SIZE == 5
 *
 * Ending state:
 * [1][2][4][5]
 * HEAD == [1]
 * neNodeSearchWithoutObject (UnitTestList.c:548)
 *
 * TAIL == [5]
 * SIZE == 4
 * EXPECTED == 3
 */
int removeFromListWithMultiNodes()
{
    struct node *node,*tail,*head;

    head = createTestNode(1);
    struct node *temp = createTestNode(2);
    node = createTestNode(3);
    struct node *temp2 = createTestNode(4);
    tail = createTestNode(5);

    addAtRear(testlist,head);
    addAtRear(testlist, temp);
    addAtRear(testlist,node);
    addAtRear(testlist, temp2);
    addAtRear(testlist,tail);

    struct node *ret = removeNode(testlist,node);
    //List state
    myassert(testlist->size == 4);
    //Head connection
    myassert(testlist->head == head);
    //Tail connection
    myassert(testlist->tail == tail);
    myassert(ret == node);
    printList(testlist);
	freeNode(ret, testlist->freeObject);
    return 1;
}

/*
 * TEST: oneNodeSearchWithObject
 * Beginning state:
 * [1]
 * HEAD == [1]
 * TAIL == [1]
 * SIZE == 1
 *
 * Ending state:
 * HEAD == [1]
 * TAIL == [1]
 * SIZE == 1
 * EXPECTED == 1
 */
int oneNodeSearchWithObject()
{
    struct node *node = createTestNode(1);
    addAtFront(testlist, node);

    struct node *exp = search(testlist,node->obj);
    myassert(exp->obj == node->obj);
    printList(testlist);
    return 1;
}

/*
 * TEST: oneNodeSearchWithoutObject
 * Beginning state:
 * [1]
 * HEAD == [1]
 * TAIL == [1]
 * SIZE == 1
 *
 * Ending state:
 * HEAD == [1]
 * TAIL == [1]
 * SIZE == 1
 * EXPECTED == NULL
 */
int oneNodeSearchWithoutObject()
{
    struct node *node = createTestNode(1);
    struct node *node1 = createTestNode(1234);
    addAtFront(testlist,node);

    struct node *exp = search(testlist,node1->obj);
    myassert(exp == NULL);
    printList(testlist);
	freeNode(node1, testlist->freeObject);
    return 1;
}

/*
 * TEST: twoNodeReverseListAtFront
 * Beginning state:
 * [2][1]
 * HEAD == [2]
 * TAIL == [1]
 * SIZE == 2
 *
 * Ending state:
 * [1][2]
 * HEAD == [1]
 * TAIL == [2]
 * SIZE == 2
 */
int twoNodeReverseListAtFront()
{
    struct node *node = createTestNode(1);
    struct node *node1 = createTestNode(2);
    addAtFront(testlist,node);
    addAtFront(testlist,node1);
    reverseList(testlist);

    //List state
    myassert(testlist->size == 2);
    //Head connection
    myassert(testlist->head == node);
    //Tail connection
    myassert(testlist->tail == node1);
    myassert(testlist->head->obj == node->obj);
    myassert(testlist->tail->obj == node1->obj);
    printList(testlist);
    return 1;
}

/*
 * TEST: twoNodeReverseListAtRear
 * Beginning state:
 * [1][2]
 * HEAD == [1]
 * TAIL == [2]
 * SIZE == 2
 *
 * Ending state:
 * [2][1]
 * HEAD == [2]
 * TAIL == [1]
 * SIZE == 2
 */
int twoNodeReverseListAtRear()
{
    struct node *node1 = createTestNode(1);
    struct node *node2 = createTestNode(2);
    addAtRear(testlist,node1);
    addAtRear(testlist,node2);
    reverseList(testlist);

    //List state
    myassert(testlist->size == 2);
    //Head connection
    myassert(testlist->head == node2);
    //Tail connection
    myassert(testlist->tail == node1);
    myassert(testlist->head->obj == node2->obj);
    myassert(testlist->tail->obj == node1->obj);
    printList(testlist);
    return 1;
}

/*
 * TEST: multiNodeFreeList
 * Beginning state:
 * [1][2][3][4][5]
 * HEAD == [1]
 * TAIL == [5]
 * SIZE == 5
 *
 * Ending state:
 * LIST == NULL
 */
int multiNodeFreeList()
{
    struct node *node1 = createTestNode(1);
    struct node *node2 = createTestNode(2);
    struct node *node3 = createTestNode(3);
    struct node *node4 = createTestNode(4);
    struct node *node5 = createTestNode(5);

    addAtRear(testlist,node1);
    addAtRear(testlist,node2);
    addAtRear(testlist,node3);
    addAtRear(testlist,node4);
    addAtRear(testlist,node5);

    freeList(testlist);
    //List state
    myassert(testlist == NULL);
    return 1;
}

/*
 * TEST: noNodeSizeTest
 * Beginning state:
 * HEAD == NULL
 * TAIL == NULL
 * SIZE == 0
 *
 * Ending state:
 * HEAD == NULL
 * TAIL == NULL
 * SIZE == 0
 * EXPECTED == 0
 */
int noNodeSizeTest()
{
    myassert(testlist->size == 0);
    printList(testlist);
    return 1;
}

/*
 * TEST: oneNodeSizeTest
 * Beginning state:
 * HEAD == NULL
 * TAIL == NULL
 * SIZE == 0
 *
 * Ending state:
 * HEAD == [1]
 * TAIL == [1]
 * SIZE == 1
 * EXPECTED == 1
 */
int oneNodeSizeTest()
{
    struct node *node = createTestNode(1);
    addAtFront(testlist,node);

    myassert(testlist->size == 1);
    printList(testlist);
    return 1;
}

/*
 * TEST: twoNodeSizeTest
 * Beginning state:
 * HEAD == NULL
 * TAIL == NULL
 * SIZE == 0
 *
 * Ending state:
 * [1][2]
 * HEAD == [1]
 * TAIL == [2]
 * SIZE == 2
 * EXPECTED == 2
 */
int twoNodeSizeTest()
{
    struct node *node = createTestNode(1);
    struct node *node2 = createTestNode(2);
    addAtFront(testlist,node);
    addAtFront(testlist,node2);

    myassert(testlist->size == 2);
    printList(testlist);
    return 1;
}

/*
 * TEST: noNodeIsEmpty
 * Beginning state:
 * HEAD == NULL
 * TAIL == NULL
 * SIZE == 0
 *
 * Ending state:
 * HEAD == NULL
 * TAIL == NULL
 * SIZE == 0
 * EXPECTED == 1 (true)
 */
int noNodeIsEmpty()
{
    int val = isEmpty(testlist);
    myassert(val == 1);
    printList(testlist);
    return 1;
}

/*
 * TEST: oneNodeIsEmpty
 * Beginning state:
 * [1]
 * HEAD == [1]
 * TAIL == [1]
 * SIZE == 1
 *
 * Ending state:
 * HEAD == [1]
 * TAIL == [1]
 * SIZE == 1
 * EXPECTED == 0 (false)
 */
int oneNodeIsEmpty()
{
    struct node *node = createTestNode(1);
    addAtFront(testlist,node);
    int val = isEmpty(testlist);

    myassert(val == 0);
    printList(testlist);
    return 1;
}

/*
 * TEST: twoNodeIsEmpty
 * Beginning state:
 * [1][2]
 * HEAD == [1]
 * TAIL == [2]
 * SIZE == 2
 *
 * Ending state:
 * [1][2]
 * HEAD == [1]
 * TAIL == [2]
 * SIZE == 2
 * EXPECTED == 0 (false)
 */
int twoNodeIsEmpty()
{
    struct node *temp = createTestNode(1);
    struct node *temp2 = createTestNode(2);
    addAtFront(testlist,temp);
    addAtFront(testlist,temp2);

    int val = isEmpty(testlist);
    myassert(val == 0);
    printList(testlist);
    return 1;
}

void runUnitTests()
{
    int result;
    char *testName;

    testName = "addAtFrontWithNoNodes";
    beforeTest(testName);
    result = addAtFrontWithNoNodes();
    afterTest(testName, result);

    testName = "addAtFrontWithOneNode";
    beforeTest(testName);
    result = addAtFrontWithOneNode();
    afterTest(testName, result);

    testName = "addAtRearWithNoNodes";
    beforeTest(testName);
    result = addAtRearWithNoNodes();
    afterTest(testName, result);

    testName = "addAtRearWithOneNode";
    beforeTest(testName);
    result = addAtRearWithOneNode();
    afterTest(testName, result);

    testName = "removeFromListWithOneNode";
    beforeTest(testName);
    result = removeFromListWithOneNode();
    afterTest(testName, result);

    testName = "nullNodeTest";
    beforeTest(testName);
    result = nullNodeTest();
    afterTest(testName, result);

    //TODO: Add in your tests here
    testName = "removeFrontWithNoNode";
    beforeTest(testName);
    result = removeFrontWithNoNode();
    afterTest(testName,result);

    testName = "removeFrontWithOneNode";
    beforeTest(testName);
    result = removeFrontWithOneNode();
    afterTest(testName,result);

    testName = "removeFrontWithTwoNodes";
    beforeTest(testName);
    result = removeFrontWithTwoNodes();
    afterTest(testName, result);

    testName = "removeRearWithNoNode";
    beforeTest(testName);
    result = removeRearWithNoNode();
    afterTest(testName,result);

    testName = "removeRearWithOneNode";
    beforeTest(testName);
    result = removeRearWithOneNode();
    afterTest(testName,result);

    testName = "removeRearWithTwoNodes";
    beforeTest(testName);
    result = removeRearWithTwoNodes();
    afterTest(testName,result);

    testName = "removeFromListWithMultiNodes";
    beforeTest(testName);
    result = removeFromListWithMultiNodes();
    afterTest(testName,result);

    testName = "oneNodeSearchWithObject";
    beforeTest(testName);
    result = oneNodeSearchWithObject();
    afterTest(testName,result);

    testName = "oneNodeSearchWithoutObject";
    beforeTest(testName);
    result = oneNodeSearchWithoutObject();
    afterTest(testName,result);


    testName = "twoNodeReverseListAtFront";
    beforeTest(testName);
    result = twoNodeReverseListAtFront();
    afterTest(testName,result);

    testName = "twoNodeReverseListAtRear";
    beforeTest(testName);
    result = twoNodeReverseListAtRear();
    afterTest(testName,result);

//    testName = "multiNodeFreeList";
//    beforeTest(testName);
//    result = multiNodeFreeList();
//    afterTest(testName,result);


    testName = "noNodeSizeTest";
    beforeTest(testName);
    result = noNodeSizeTest();
    afterTest(testName,result);

    testName = "oneNodeSizeTest";
    beforeTest(testName);
    result = oneNodeSizeTest();
    afterTest(testName,result);

    testName = "twoNodeSizeTest";
    beforeTest(testName);
    result = twoNodeSizeTest();
    afterTest(testName,result);

    testName = "noNodeIsEmpty";
    beforeTest(testName);
    result = noNodeIsEmpty();
    afterTest(testName,result);

    testName = "oneNodeIsEmpty";
    beforeTest(testName);
    result = oneNodeIsEmpty();
    afterTest(testName,result);

    fprintf(stdout, "Test Cases: %d\n",  testCount);
    fprintf(stdout, "Passed: %d\n", passCount);
    fprintf(stdout, "Failed: %d\n", testCount - passCount);
}

int main(int argc, char *argv[])
{
    	runUnitTests();
    	exit(0);
}
