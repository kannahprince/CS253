#!/bin/bash

#Check for user file
if [ "$1" = "" ];then
  echo "usage: $0 <output file>"
  echo "   output file - the file to save the grades in"
  exit 0;
fi
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./lib
make
./wf --self-organized-list sample-input.txt >> result
echo "HW5:PASS" >> $1 #Write the result to the specified file

#Clean up
make clean
rm -f result
