# Project 5: Shell Scripting & Using a Library

* Author: Prince Kannah
* Class: CS253 Section 3

## Overview:

In project 5 we write a makefile to link to existing code and add shell scripting to automatetesting.

## Compiling and Using:

* wf.c
* sample-input.txt
* WordObj.h
* WordObj.c
* backpack.sh - auto grader use to check the correctness of the Makefile.
* Makefile - gcc Makefile

**_NOTE:_ All program files should be in the same directory.**

To compile and run the program (using terminal):
`make`

Then: 
./backpack.sh <output-file>

## Discussion:

One challenge I did have was figuring out the logic. While it’s pretty easy to describe what it should do it prove a bit tricky programming it. That always seems to be the case. The previous project was really helpful when it came to writing the Makefile. I looked at other backpack scripts when writing my.

## Testing:

Testing was a constant part of this project. With each new modification I'd run my program to see the results and going back to fix it if it didn't quite look right. One of the biggest mistake I ran into was NullPointer exceptions. As the program got more complex the error got more prevalent. So, as good practice, I always initialize my variables to some values.

## Sources used:

I utilized the Piazza posts to help me when I got stuck.

