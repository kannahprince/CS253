# Project 1: Word Counting in C

* Author: Prince Kannah
* Class: CS253 Section 3

## Overview:

As the title of the project suggests, the main object is read input from stdin, going through
to find the number of words,characters, lines and digits 0-9. The result is then printed to stdout.

## Compiling and Using:

The files included with this program:
* main.c — source code to do the word counting.
* README.md - contains instructions on running the program and serves as a 
development diary.

All program files must be in the same directory before compiling and running:
* To compile (using gcc):
    * gcc main.c

* To run the compiled program:
    * ./a.out < input-file

## Discussion:

This project is reminiscent of Text Statistics from CS121. In that one, unlike this, we had to process a file
and find the number of words among other things.
Going from Java to C is a bit strange. By far the biggest challenge was putting aside some of Java's OOP
paradigm. For example, I wanted to write a function to do the digit counting instead of putting everything in my main function. I wasn't quite able to get it working because (after doing some searches) I learned I needed to pass a pointer.

Another minor problem I had was defining constants. I initially had ended my declaration with a semicolon (;). When I use one of the constants I got a complier error. After reviewing _K&R_, I realized I didn't need to add the semicolon on constants as is the case in Java.

I used the provided data file to test my program. After verifying that it was working as expected I try other files, including the Gutenberg encyclopedia.

## Sources used:

I used the word counting examples in the class repository and the examples in chapter one of _K&R_. 
