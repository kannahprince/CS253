#include <stdio.h>
#define IN_WORD 0
#define OUT_WORD 1
#define SPACE ' '
#define TAB '\t'
#define NEW_LINE '\n'
int const DIGIT_MAX = 10;
/*Counts the number words, characters, line and digits in the standard input stream.*/
int main() {
    int character, wordStatus;
    long numLines = 0;
    long numWords = 0;
    long numCharacters =0;
    int digits[DIGIT_MAX];

    wordStatus = OUT_WORD;

    /*set default values for array indices to zero*/
    for (int j = 0; j < DIGIT_MAX; ++j) {
        digits[j] = 0;
    }
	
	/*Counting number of words, lines and characters*/
    while ((character = getchar()) != EOF){
        numCharacters++;

        if(character == NEW_LINE)
            numLines++;

        if(character == SPACE || character == NEW_LINE|| character == TAB){
            wordStatus = OUT_WORD;
        }
        else if (wordStatus == OUT_WORD) {
            wordStatus = IN_WORD;
            numWords++;
        }

        /*Counting number of digits */
        if (character >= '0' && character <= '9'){
            digits[character - '0']++;
        }
    }

    printf("words: %ld\nchars: %ld\nlines: %ld\n", numWords, numCharacters,numLines);
	
	/*Print the contents of the digits array*/
    for (int i = 0; i < 10 ; ++i) {
            printf("digit %d: %d\n",i,digits[i]);
    }
    return 0;
}

