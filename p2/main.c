#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <stdlib.h>

/*Use to identify possible command line argument errors.*/
enum ERROR_CAUSE{NO_ARGS,OUT_OF_RANGE, TOO_MANY_ARGS};

__const int TOTAL_ARGS_COUNT = 3;
__const int BIT = 8;
__const int BYTES = (8 * sizeof(unsigned int));

/*Checks for and returns the set bit.*/
unsigned int isBitSet(int p, int j)
{
    return ((1U << j) & p);
}

/* Prints all n bits in the argument x.*/
void printBits(unsigned int x)
{
    int numBits = sizeof(unsigned int) * 8;

    for(int j = numBits - 1; j >= 0; j--)
    {
        if(isBitSet(x,j))
        {
            printf("1");
        }
        else
            printf("0");
    }
}

/*Converts the binary char to its decimal value*/
unsigned int btoi(char binaryString[])
{
    unsigned int temp = 0;
    int length = (int)strlen(binaryString);

    for(int i = 0; i < length;)
    {
        if(binaryString[i] == '1')
        {
            temp = temp | (1U << (length - 1 - i));
        }
        i++;
    }
    return temp;
}

/*Converts the endianess of the argument x using bitwise operations*/
unsigned int swapBytes(unsigned int x)
{
    unsigned int mask = 0xff; 
	unsigned int rev= 0;
	for(int i = 0; i < sizeof(unsigned int); i++)
	{
		int byte = (x >> ((sizeof(unsigned int)-1 -i) * 8)) & mask;
		rev |= byte << (i * 8); 
	}

    printf("x' %-10u\t", rev);
    printBits(rev);
    printf("    0x%.8x\n", rev);
    return x;
}

/*Prints a error message prepended with the char[] to stderr and exit.*/
void printErrorMessage(enum ERROR_CAUSE cause, char string[]){
    switch (cause){
        case NO_ARGS:
            fprintf(stderr,"No valid arguments provided\nUsage: %s -i|-b\n"
                            " -i x: positive integer less %u\n"
                            " -b x: binary representation of positive integer less than %u\n\t\t"
                    "(string of 1s and 0s)\n",string,UINT_MAX, UINT_MAX);
            exit(1);
        case OUT_OF_RANGE:
            fprintf(stderr,"The integer must be in the range [0-"
                    "%u]\nUsage: %s -i|-b\n"
                    " -i x: positive integer less %u\n"
                    " -b x: binary representation of positive integer less than %u\n\t\t"
                    "(string of 1s and 0s)\n",UINT_MAX,string,UINT_MAX,UINT_MAX);
            exit(1);
        case TOO_MANY_ARGS:
            fprintf(stderr,"Argument count exceed expected\n"
                    "Usage: %s -i|-b\n -i x: positive integer less %u\n"
                    " -b: binary representation of positive integer less than %u\n\t\t"
                    "(string of 1s and 0s)\n",string, UINT_MAX, UINT_MAX);
            exit(1);
        default:
           fprintf(stderr,"Usage: %s -i|-b\n -i x: positive integer less than%u\n"
                   " -b x: binary representation of positive integer less than %u\n\t\t"
                   "(string of 1s and 0s)\n",string,UINT_MAX, UINT_MAX);
            exit(1);
    }
}

/*main function, runs the program.*/
int main(int argc, char *argv[])
{
    unsigned int x;
    unsigned long checker;

    if(argc < TOTAL_ARGS_COUNT){
        printErrorMessage(NO_ARGS, argv[0]);
    }
    else if(argc > TOTAL_ARGS_COUNT){
        printErrorMessage(TOO_MANY_ARGS,argv[0]);
    }
    else
    {
        if(strcmp(argv[1], "-i") == 0)
        {
            checker = atol(argv[2]);
            if(checker > UINT_MAX){
                printErrorMessage(OUT_OF_RANGE,argv[0]);
            }
            else
            {
                x = (unsigned int)atol(argv[2]);
                printf("Decimal      \tBinary                              Hexadecimal\n");
                printf("----------------------------------------------------------------\n");
                printf("x  %-10u\t",x);
                printBits(x);
                printf("    0x%.8x\n", x);
                swapBytes(x);
            }
        }
        else if(strcmp(argv[1], "-b") == 0)
        {
            x = btoi(argv[2]);

            printf("Decimal      \tBinary                              Hexadecimal\n");
            printf("----------------------------------------------------------------\n");
            printf("x  %-10u\t",x);
            printBits(x);
            printf("    0x%.8x\n", x);
            swapBytes(x);
        }
    }
    return 0;
}
