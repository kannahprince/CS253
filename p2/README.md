# Project 2: Fun with Bits

* Author: Prince Kannah
* Class: CS253 Section 3

## Overview:

The main objective of P2 was to get even more familiar with C, especially the bitwise operations
use to manipulate data at the bit level. The program works as follow: a user inputs either a binary or 
decimal value then all set bits are printed along with hexadecimal representation to stdout. The endianness
are is swapped.

## Compiling and Using:

The files included with this program:
* main.c - source code
* Makefile - gcc makefile
* backpack.sh - auto grader use to check the correctness of the application.
* data - contains binary and decimal values to test the program against.
* README.md - contains instructions on running the program and serves as a 
development diary.

All program files must be in the same directory before compiling and running:
* To compile (using gcc):
    * gcc main.c

* To run the compiled program:
    * ./a.out -i|-b [ integer value | binary value ]

## Discussion:

One of the biggest issue I had with this project was understanding bitwise operations and what the do to data that
they operate on. I had a hard time grasping what && or | a number would do. And for a while I wasn't quite sure 
why there would ever be a need to do any sort of bit shifting. After several lectures, the professor was able
to make it clear that bit shifting is important in cryptography and in cases where resources, especially
memory is limited.

C is still weird to me but I believe I'm starting to get the hang. For the first part of the assignment I used an enum to list all my possible command line errors. I then use a method that switches on the enum parameter to print the correct error message. I was initially having issues with command-line validation because I was using 
atol() but then casting to an int, causing me to loose precision. I have one known bugs in my program:
* Binary string input bound validation isn't quite up to snuff.

## Testing:

For testing, I ran my program through the auto grader and once I passed I inputted various integer values and binary strings to check that the program was indeed working not only for the grader but was capable of working on any input. This lead me to discover the two bugs discuessed above. I'm not sure to resolve both at the moment.

I think the assignment should be renamed to Little fun with Bits because there's no fun to be had with bit shifting :D


## Sources used:

I utilized the Piazza posts to help me when I got stuck and use the isBitSet() function from the class notes
and borrow from the getBits() function in _K&R_ as well as any notes made available in class or through the class repository.
