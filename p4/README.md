# Project 4: Makefile

* Author: Prince Kannah
* Class: CS253 Section 3

## Overview:

In P4, we write a simple Make file with multiple targets all with the end goal of being familiar 
with make and Makefiles.

## Compiling and Using:

The files included in this projects are:
* main.c - the Makefile is use to compile and run this.
* backpack.sh - auto grader use to check the correctness of the Makefile.
* Makefile - gcc Makefile

**_NOTE:_ All program files should be in the same directory.**

To compile and run the program (using terminal):
`make`

## Discussion:

All the previous projects had Makefile so it's pretty cool to get to write my own. To get an idea of what I needed to include I looked at the Makefiles in those previous projects and try to emulate them. Everything was confusing at first however, once I understood the interaciton between targets and dependencies I was able to sucessfully write my Makefile. The part that took me a while to figure out was the make run target. At first I simply ran ./hello however if make is never call that file is not there, leading to an error. At his point I already had a hello target, all I needed to do was simply make run _depend_ on it. This forces the hello target to run, producing the executable that is need by run!

## Testing:

Before running the auto-grader I indepently tested each make target to make sure the dependencies were in order. I then manually ran the executable that was produced (It worked!). I then clean up everything before running the auto-grader.

## Sources used:

 utilized the Piazza posts to help me when I got stuck. No other resouces outside of class notes and lectures were used.
