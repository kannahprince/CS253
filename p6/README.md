# Project 6: Threads & Signal Part 1

* Author: Prince Kannah
* Class: CS253 Section 3

## Overview:

In proect 6, we write and test a single threaded buffer to write a program's log to a log file. Linux system signals are use to help to that effect. We also incoorporate a lot of P4 & P5 with `make` and using libraries.

## Compiling and Using:

The included files are listed in the directory tree below:

	├── backpack.sh
	├── doxygen-config
	├── include
	├── lib
	├── Makefile
	├── README
	├── README.md
	├── run.sh
	├── src
	│   ├── Makefile
	│   ├── ring.c
	│   ├── ring.h
	└── test
    	├── Makefile
    	└── test.c

**_NOTE:_ All program files should be in the same directory.**

To compile and run the program (using terminal):
1. `cd ring-buffer`
2. `make`

Then: 
`./backpack.sh <output-file>`

You can also use the run.sh script to run the test file:
`./run.sh`

## Discussion:

One challenge I did have was understanding the the interaction between my program and Linux's signals. While it’s pretty easy to describe what it should do it prove a bit tricky programming it. That always seems to be the case. One of the biggest mistake I ran into was NullPointer exceptions. As the program got more complex the error got more prevalent. So, as good practice, I always initialize my variables to some values.

## Testing:

With each new modification I'd run my program to see the results and going back to fix it if it didn't quite look right. One of the biggest challenge I had on this project as understanding signals and how they interacted with my program. It was while before I fully understood how it really worked. Once I think I understood them, I went back and looked at the example codes and they made a lot more sense and I was able to implement the idea in my code.

## Sources used:

I utilized the Piazza posts to help me when I got stuck. And I also looked at these webpages for help:
	
* [Writing to file in C](http://stackoverflow.com/questions/11573974/write-to-txt-file)
* [Linux man page on signals](http://man7.org/linux/man-pages/man7/signal.7.html)
* [FreeBSD man page on signals](https://www.freebsd.org/cgi/man.cgi?sektion=3&query=signal)
