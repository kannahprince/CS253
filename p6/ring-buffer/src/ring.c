#include <string.h>
#include <stdio.h>
#include "ring.h"
#include <unistd.h>
#include <signal.h>

static struct {
    int curr;
    char log[MAX_LOG_ENTRY][MAX_STRING_LENGTH];
} buff;

void init_buffer()
{
    printf("Initialize the ring buffer\n");
    int i;
    for(i = 0; i < MAX_LOG_ENTRY; i++) {
        buff.log[i][0] = '\0';
    }
}

void log_msg(char *entry)
{
	//check for null string
	if(entry == NULL) return;
    
	printf("Adding log entry into buffer\n");
    int idx = buff.curr % MAX_LOG_ENTRY;
    strncpy(buff.log[idx],entry,MAX_STRING_LENGTH);
    /*
     * From the documentation of strncpy:
     * No null-character is implicitly appended at the end of destination
     * if source is longer than num. Thus, in this case, destination shall
     * not be considered a null terminated C string
     * (reading it as such would overflow).
     *
     * Thus we need to make sure that we null terminate the string;
     */
	if(buff.curr == MAX_LOG_ENTRY) buff.curr = 0;
    
	buff.log[idx][MAX_STRING_LENGTH-1] = '\0'; //null terminate super long strings
    buff.curr++;

	signal(SIGALRM,check_point);
	alarm(alarm_interval);
}

/*
 * This method writes all the current entries to disk.
 */
static void dump_buffer()
{
	FILE *fp = fopen(log_name, "w+"); //open file and set writing to overwrite
    for(int i = 0; i < MAX_LOG_ENTRY; i++) {
		fprintf(fp,"log %d: %s\n", i, buff.log[i]);
    }
}

void check_point(){
	dump_buffer(); 
}
